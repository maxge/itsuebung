import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
        Main main = new Main();
        String encryptedECB = main.encryptCaesar("ITSICHERHEIT", 2, "ECB", 0);
        System.out.println(main.decryptCaesar(encryptedECB, 2, "ECB", 0));
        String encryptedCBC = main.encryptCaesar("ITSICHERHEIT", 2, "CBC", 4);
        System.out.println(main.decryptCaesar(encryptedCBC, 2, "CBC", 4));

        String encryptedECB2 = main.encryptCaesar("SUPER TEST FUER A BIS Z", 2, "ECB", 0);
        System.out.println(main.decryptCaesar(encryptedECB2, 2, "ECB", 0));
        String encryptedCBC2 = main.encryptCaesar("SUPER TEST FUER A BIS Z", 2, "CBC", 4);
        System.out.println(main.decryptCaesar(encryptedCBC2, 2, "CBC", 4));

        String aufgabe63a = "KPL DBLYKL KLZ TLUZJOLU PZA BUHUAHZAIHY ZPL GB HJOALU BUK GB ZJOBLAGLU PZA CLYWMSPJOABUN HSSLY ZAHHASPJOLU NLDHSA";
        String aufgabe63b = "RSF OPGQVBWHH BIHNH RWS PIQVGHOPSB B IBR H PSGCBRSFG CTH IBR KSWQVH RSGVOZP JCA RIFQVGQVBWHH OP ROG RSQVWTTFWSFSB WGH RSBBCQV WA BIZZYCAAOBWQVHG RIFQV QCADIHSF AOQVPOF";
        System.out.println(main.decryptCaesar(aufgabe63a, 7, "ECB", 0));
        System.out.println(main.decryptCaesar(aufgabe63b, 14, "ECB", 0));

        for(int i = 0; i < 26; i ++) {
            //System.out.println(main.decryptCaesar("RSF OPGQVBWHH BIHNH RWS PIQVGHOPSB B IBR H PSGCBRSFG CTH IBR KSWQVH RSGVOZP JCA RIFQVGQVBWHH OP ROG RSQVWTTFWSFSB WGH RSBBCQV WA BIZZYCAAOBWQVHG RIFQV QCADIHSF AOQVPOF", i, "ECB", 0));
        }

        LinkedHashMap<Character, Integer> lf = main.letterf(aufgabe63a);
        lf.entrySet().stream().sorted(Map.Entry.comparingByValue()).forEach(entry -> System.out.println(entry) );
    }

    private String encryptCaesar(String text, int key, String mode, int iv) {
        String cipherText = "";
        if(mode.equalsIgnoreCase("ECB")) {
            for (char c : text.toCharArray()) {
                c = (char) ((c + key) % 91);
                if(c < 65) {
                    c += 65;
                }
                cipherText += c;
            }
        } else {
            char old = (char)(iv + 65);
            for (char c : text.toCharArray()) {
                c = (char) (((c + (old - 65)) + key) % 91);
                if(c < 65) {
                    c += 65;
                }
                cipherText += c;
                old = c;
            }
        }
        return cipherText;
    }

    private String decryptCaesar(String text, int key, String mode, int iv) {
        String plainText = "";
        if(mode.equalsIgnoreCase("ECB")) {
            for (char c : text.toCharArray()) {
                c = (char) ((c - key) % 91);
                if(c < 65) {
                    c += 26;
                }
                plainText += c;
            }
        } else {
            char old = (char)(iv + 65);
            for (char c : text.toCharArray()) {
                char t = c;
                c = (char) (((c - (old - 65)) - key) % 91);
                if(c < 65) {
                    c += 26;
                }
                plainText += c;
                old = t;
            }
        }
        return plainText;
    }

    private LinkedHashMap<Character, Integer> letterf(String text) {
        LinkedHashMap<Character, Integer> lf = new LinkedHashMap<Character, Integer>();
        for (char c : text.toCharArray()) {
            if(lf.get(c) == null && c != 32) {
                lf.put(c, 1);
            } else if( c != 32 && lf.get(c) > 0) {
                lf.put(c, lf.get(c) + 1);
            }
        }
        return lf;
    }
}
